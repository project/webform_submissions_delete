## Introduction
This module is used to delete Webform submissions in
bulk using a specified date range.

This module will create a tab/link under the Webform
Results tab, which will allow a user to delete
submissions in bulk.

## Requirements
This module requires the Webform module:
https://www.drupal.org/project/webform

## Installation/Usage
1. Install module like any other contrib module.
2. Select any Webform with submissions.
3. Click on the "Results" tab.
4. Locate the "Bulk Delete" sub tab.
5. Select date range for bulk deletion of submissions.
6. Confirm submissions have been deleted.

## Maintainers

George Anderson (geoanders)
https://www.drupal.org/u/geoanders
