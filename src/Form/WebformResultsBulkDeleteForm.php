<?php

namespace Drupal\webform_submissions_delete\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Form\WebformResultsClearForm;

/**
 * Bulk delete for webform submissions.
 */
class WebformResultsBulkDeleteForm extends WebformResultsClearForm {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'results_bulk_delete_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if ($submission_total = $this->submissionStorage->getTotal($this->webform, $this->sourceEntity)) {
      $form['container'] = [
        '#type' => 'container',
        '#attributes' => ['class' => ['container-inline']],
      ];

      $form['container']['delete_start_date'] = [
        '#title' => $this->t('Start Date'),
        '#type' => 'date',
        '#date_year_range' => '-10:+10',
        '#date_format' => 'Y-m-d',
      ];

      $form['container']['delete_end_date'] = [
        '#title' => $this->t('End Date'),
        '#type' => 'date',
        '#date_year_range' => '-10:+10',
        '#date_date_format' => 'd/m/Y',
      ];

      $form['container']['delete_submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Delete submissions'),
      ];

      return $form;
    }
    else {
      $t_args = ['%label' => $this->getLabel()];
      $form['message'] = [
        '#type' => 'webform_message',
        '#message_type' => 'error',
        '#message_message' => $this->t('There are no %label submissions.', $t_args),
      ];

      return $form;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $start_date = $form_state->getValue('delete_start_date');
    $end_date = $form_state->getValue('delete_end_date');

    if ($start_date == '') {
      $form_state->setErrorByName('delete_start_date', $this->t('Please enter a valid start date.'));
    }

    if ($end_date == '') {
      $form_state->setErrorByName('delete_end_date', $this->t('Please enter a valid end date.'));
    }

    if (strtotime($start_date) > strtotime($end_date)) {
      $form_state->setErrorByName('delete_start_date', $this->t('Start date should not be greater than end date.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $start_date = strtotime($form_state->getValue('delete_start_date'));
    $end_date = $form_state->getValue('delete_end_date');

    if ($end_date != '' && ($end_date = strtotime($end_date)) !== FALSE) {
      if ($end_date == strtotime('midnight', $end_date)) {
        // Full day specified.
        $end_date += 86399;
      }
    }

    $count = $this->getTotal($start_date, $end_date);
    $batchLimit = $this->getBatchLimit();
    if ($count < $batchLimit) {
      $this->deleteSubmissions($start_date, $end_date);
      $this->messenger()->addStatus($this->getFinishedMessage());
    }
    else {
      $this->deleteBatchSet($start_date, $end_date, $count, $batchLimit);
    }
  }

  /**
   * Get total submissions that fall in date range.
   *
   * @param string $startDate
   *   Start date.
   * @param string $endDate
   *   End date.
   *
   * @return array|int
   *   Returns total submissions.
   */
  private function getTotal($startDate, $endDate) {
    $entity_query = $this->submissionStorage->getQuery();
    $entity_query->condition('webform_id', $this->webform->id(), '=');
    $entity_query->condition('created', $startDate, '>=');
    $entity_query->condition('created', $endDate, '<=');
    $result = $entity_query->count()->execute();
    return $result;
  }

  /**
   * Delete submissions.
   *
   * @param string $startDate
   *   Start date.
   * @param string $endDate
   *   End date.
   * @param int $limit
   *   Limit number of submissions.
   *
   * @return int
   *   Returns count of submissions.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function deleteSubmissions($startDate, $endDate, $limit = NULL): int {
    $entity_query = $this->submissionStorage->getQuery();
    $entity_query->condition('webform_id', $this->webform->id(), '=');
    $entity_query->condition('created', $startDate, '>=');
    $entity_query->condition('created', $endDate, '<=');
    $entity_query->sort('sid');
    if ($limit) {
      $entity_query->range(0, $limit);
    }
    $entity_ids = $entity_query->execute();
    $entities = $this->submissionStorage->loadMultiple($entity_ids);
    $this->submissionStorage->delete($entities);
    return count($entities);
  }

  /**
   * Bulk delete batch set.
   *
   * @param string $start_date
   *   The start date.
   * @param string $end_date
   *   The end date.
   * @param int $count
   *   The count.
   * @param int $batchLimit
   *   The batch limit.
   */
  public function deleteBatchSet($start_date, $end_date, $count, $batchLimit) {
    $totalBatch = (int) $count / $batchLimit;
    $reminder = $count % $batchLimit;
    if ($reminder > 0) {
      $totalBatch++;
    }
    $operations = [];
    for ($i = 0; $i < $totalBatch; $i++) {
      $operations[] = [
        [$this, 'deleteBatchProcess'],
        [
          $start_date,
          $end_date,
          $batchLimit,
        ],
      ];
    }
    $batch = [
      'title' => $this->t('Clear submissions'),
      'init_message' => $this->t('Clearing submission data'),
      'error_message' => $this->t('The submissions could not be cleared because an error occurred.'),
      'operations' => $operations,
      'finished' => [$this, 'batchFinish'],
    ];
    batch_set($batch);
  }

  /**
   * Bulk delete batch process.
   *
   * @param string $start_date
   *   The start date.
   * @param string $end_date
   *   The end date.
   * @param int $batchLimit
   *   The batch limit.
   * @param array $context
   *   The context.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteBatchProcess($start_date, $end_date, $batchLimit, array &$context) {
    $this->deleteSubmissions($start_date, $end_date, $batchLimit);
    $context['message'] = $this->t('Deleting submissions');
  }

}
